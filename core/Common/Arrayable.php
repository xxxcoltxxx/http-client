<?php

namespace Core\Common;

interface Arrayable
{
    public function toArray(): array;
}
