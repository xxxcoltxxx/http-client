<?php

namespace Core\Http\Client\Serializers;

interface SerializerContract
{
    public function parse($content);
}
