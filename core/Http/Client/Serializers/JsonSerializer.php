<?php

namespace Core\Http\Client\Serializers;

use Core\Http\Client\Exceptions\HttpResponseParseException;

class JsonSerializer implements SerializerContract
{
    public function parse($content)
    {
        $json = json_decode($content, true);
        if ($json === false) {
            throw new HttpResponseParseException("Response does not contain json", $content);
        }

        return $json;
    }
}
