<?php

namespace Core\Http\Client\Exceptions;

use Exception;

class HttpResponseParseException extends Exception
{
    protected $content = '';

    public function __construct(string $message = "", $content = null)
    {
        parent::__construct($message, 400);
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
