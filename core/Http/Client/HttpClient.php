<?php

namespace Core\Http\Client;

use Core\Http\Client\Exceptions\HttpRequestException;
use Core\Http\Client\Serializers\JsonSerializer;
use Core\Http\Client\Serializers\SerializerContract;
use Psr\Log\LoggerInterface;

class HttpClient implements HttpClientContract
{
    protected $config = [];

    /**
     * @var SerializerContract
     */
    protected $serializer;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(string $host, int $port, int $timeoutMs = 5, int $thresholdMs = 5)
    {
        $this->config = [
            'host'        => rtrim($host, '/'),
            'port'        => $port,
            'timeoutMs'   => $timeoutMs,
            'thresholdMs' => $thresholdMs,
        ];

        // By default
        $this->serializer = new JsonSerializer();
    }

    public function setSerializer(SerializerContract $serializer)
    {
        $this->serializer = $serializer;
    }

    public function get(string $url, array $queryParams = [], array $headers = [])
    {
        $fullUrl = $url . ($queryParams ? http_build_query($queryParams) : '');

        return $this->request('GET', $fullUrl, [], $headers);
    }

    public function post(string $url, array $params = [], array $headers = [])
    {
        return $this->request('POST', $url, $params, $headers);
    }

    public function put(string $url, array $params = [], array $headers = [])
    {
        return $this->request('PUT', $url, $params, $headers);
    }

    public function delete(string $url, array $params = [], array $headers = [])
    {
        return $this->request('DELETE', $url, $params, $headers);
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    protected function request(string $method, string $url, array $params, array $headers)
    {
        $url = ltrim($url, '/');
        $fullUrl = "{$this->config['host']}:{$this->config['port']}/{$url}";

        $curl = curl_init($fullUrl);
        curl_setopt_array($curl, array_merge([
            CURLOPT_TIMEOUT_MS     => $this->config['timeoutMs'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_POSTFIELDS     => $params,
        ]));

        $time = microtime(true);
        $result = curl_exec($curl);
        $time = microtime(true) - $time;

        if ($time > $this->config['thresholdMs']) {
            $this->log($time, $method, $fullUrl, $params, $headers);
        }

        if ($result === false) {
            throw new HttpRequestException(curl_error($curl), curl_errno($curl));
        }

        return $this->serializer->parse($result);
    }

    protected function log(float $time, string $method, string $fullUrl, array $params, array $headers)
    {
        if ($this->logger) {
            $this->logger->warning("[{$method}] {$fullUrl} request time: {$time}");
        }
    }
}
