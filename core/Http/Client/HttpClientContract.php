<?php

namespace Core\Http\Client;

use Core\Http\Client\Serializers\SerializerContract;

interface HttpClientContract
{
    public function setSerializer(SerializerContract $serializer);

    public function get(string $url, array $queryParams = [], array $headers = []);

    public function post(string $url, array $params = [], array $headers = []);

    public function put(string $url, array $params = [], array $headers = []);

    public function delete(string $url, array $params = [], array $headers = []);
}
