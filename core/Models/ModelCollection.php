<?php

namespace Core\Models;

use Core\Common\Arrayable;

class ModelCollection implements Arrayable
{
    protected $models;

    public function __construct(array $models)
    {
        $this->models = $models;
    }

    public function toArray(): array
    {
        return array_map(function (Model $model) {
            return $model->toArray();
        }, $this->models);
    }
}
