<?php

namespace Core\Models;

use Core\Common\Arrayable;
use InvalidArgumentException;

abstract class Model implements Arrayable
{
    public $fillable = [];

    protected $attributes = [];

    /**
     * @var Model[]|ModelCollection[]
     */
    protected $relations = [];

    public function __construct($attributes = [])
    {
        return $this->hydrate($attributes);
    }

    public function hydrate($attributes = [])
    {
        if (! is_array($attributes)) {
            return $this;
        }

        $this->attributes = array_filter($attributes, function ($name) {
            return in_array($name, $this->fillable);
        }, ARRAY_FILTER_USE_KEY);

        return $this;
    }

    /**
     * @param string $name
     * @param Model|ModelCollection|Model[] $relation
     */
    public function setRelation(string $name, $relation)
    {
        if (is_array($relation)) {
            $this->relations[$name] = new ModelCollection($relation);
            return;
        }

        if ($relation instanceof ModelCollection || $relation instanceof Model) {
            $this->relations[$name] = $relation;
            return;
        }

        if (is_null($relation)) {
            $this->unsetRelation($name);
        }

        throw new InvalidArgumentException('Invalid relation type');
    }

    public function unsetRelation(string $name)
    {
        unset($this->relations[$name]);
    }

    public function toArray(): array
    {
        return array_merge(
            $this->attributes,
            array_map(function (Arrayable $relation) {
                return $relation->toArray();
            }, $this->relations)
        );
    }

    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    public function __get($name)
    {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }

        if (isset($this->relations[$name])) {
            return $this->relations[$name];
        }

        return null;
    }

    public function __isset($name)
    {
        return isset($this->attributes[$name]);
    }
}
