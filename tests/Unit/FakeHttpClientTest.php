<?php

namespace Tests\Unit;

use App\FakeHttpClient;
use App\Models\Address;
use App\Models\User;
use Core\Http\Client\HttpClient;
use Mockery;
use Tests\TestCase;

class FakeHttpClientTest extends TestCase
{
    /**
     * @var Mockery\MockInterface
     */
    protected $client;

    /**
     * @var FakeHttpClient
     */
    protected $fakeHttpClient;

    protected $responseMock = [
        'id'        => 1,
        'name'      => 'Test',
        'last_name' => 'Petrov',
        'address'   => [
            'id'       => 10,
            'country'  => 'Russia',
            'iso_code' => 'ru',
            'city'     => 'Moscow',
        ],
    ];

    protected function setUp()
    {
        parent::setUp();
        $this->client = Mockery::mock(HttpClient::class);
        $this->fakeHttpClient = new FakeHttpClient($this->client);
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->assertEquals(1, Mockery::getContainer()->mockery_getExpectationCount());
        Mockery::close();
    }

    public function test_get()
    {
        $this->client->shouldReceive('get')->once()->withArgs(['users/1'])->andReturn($this->responseMock);

        $user = $this->fakeHttpClient->get(1);

        $this->assertInstanceOf(User::class, $user);
        $this->assertInstanceOf(Address::class, $user->address);
        $this->assertEquals($user->toArray(), $this->responseMock);
    }

    public function test_update()
    {
        $user = new User($this->responseMock);
        $user->setRelation('address', new Address($this->responseMock['address']));

        $responseMock = $this->responseMock;
        $responseMock['name'] = 'New name';
        $responseMock['address']['city'] = 'Irkutsk';
        $this->client->shouldReceive('put')->once()->withArgs(['users/1', $responseMock])->andReturn($responseMock);

        $user->name = $responseMock['name'];
        $user->address->city = $responseMock['address']['city'];
        $this->fakeHttpClient->update($user);

        $this->assertInstanceOf(User::class, $user);
        $this->assertInstanceOf(Address::class, $user->address);
        $this->assertEquals($user->toArray(), $responseMock);
    }

    public function test_delete()
    {
        $this->client->shouldReceive('delete')->once()->withArgs(['users/1']);
        $this->fakeHttpClient->delete(1);
    }

    public function test_store()
    {
        $userMock = $this->responseMock;
        unset($userMock['id'], $userMock['address']['id']);
        $user = new User($userMock);
        $user->setRelation('address', new Address($userMock['address']));
        $this->client->shouldReceive('post')->once()->withArgs(['users', $userMock])->andReturn($this->responseMock);

        $this->fakeHttpClient->store($user);

        $this->assertInstanceOf(User::class, $user);
        $this->assertInstanceOf(Address::class, $user->address);
        $this->assertEquals($user->toArray(), $this->responseMock);
    }
}
