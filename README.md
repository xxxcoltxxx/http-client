[![pipeline status](https://gitlab.com/xxxcoltxxx/http-client/badges/master/pipeline.svg)](https://gitlab.com/xxxcoltxxx/http-client/commits/master)

![http-client_720](/uploads/f7875b529b4856fcbc7d62360c030abb/http-client_720.mov)

# Installation
```sh
git clone git@gitlab.com:xxxcoltxxx/http-client.git
cd http-client
composer install
```

# Example
```sh
php run.php
```

# Run tests
```sh
vendor/bin/phpunit tests
```