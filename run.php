<?php

use App\FakeHttpClient;
use App\Models\Address;
use App\Models\User;
use Core\Http\Client\HttpClient;

// composer, или своя функция spl_autoload_register для psr-4 и еще многого другого
require_once "vendor/autoload.php";

$client = new HttpClient('http://localhost', 81);

$fake = new FakeHttpClient($client);

$user = new User([
    'name'      => 'Test',
    'last_name' => 'Petrov',
]);

$address = new Address([
    'country'  => 'Russia',
    'iso_code' => 'ru',
    'city'     => 'Moscow',
]);

$user->setRelation('address', $address);

// Создание
$fake->store($user);

// Обновление
$user->name = 'New name';
$fake->update($user);

// Получение
$user = $fake->get($user->id);

// Удаление
$fake->delete($user->id);
