<?php

namespace App\Models;

use Core\Models\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property-read Address $address
 */
class User extends Model
{
    public $fillable = ['id', 'name', 'last_name'];
}
