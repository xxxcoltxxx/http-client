<?php

namespace App\Models;

use Core\Models\Model;

/**
 * @property int $id
 * @property string $country
 * @property string $is_code
 * @property string $city
 */
class Address extends Model
{
    public $fillable = ['id', 'country', 'iso_code', 'city'];
}
