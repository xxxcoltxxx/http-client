<?php

namespace App;

use App\Models\Address;
use App\Models\User;
use Core\Http\Client\HttpClient;
use Core\Http\Client\HttpClientContract;

class FakeHttpClient
{
    /**
     * @var HttpClient
     */
    protected $client;

    public function __construct(HttpClientContract $client)
    {
        $this->client = $client;
    }

    public function get(int $id): User
    {
        $response = $this->client->get("users/{$id}");
        $user = new User($response);
        $user->setRelation('address', new Address($response['address'] ?? null));

        return $user;
    }

    public function update(User $user): User
    {
        $response = $this->client->put("users/{$user->id}", $user->toArray());
        $user->hydrate($response);
        $address = $user->address
            ? $user->address->hydrate($response['address'] ?? null)
            : new Address($response['address'] ?? null);
        $user->setRelation('address', $address);

        return $user;
    }

    public function store(User $user): User
    {
        $response = $this->client->post('users', $user->toArray());
        $user->hydrate($response);

        $address = $user->address
            ? $user->address->hydrate($response['address'] ?? null)
            : new Address($response['address'] ?? null);

        $user->setRelation('address', $address);

        return $user;
    }

    public function delete(int $id)
    {
        $this->client->delete("users/{$id}");
    }
}
